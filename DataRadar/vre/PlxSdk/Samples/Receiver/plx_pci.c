/*
 * testMakeFile.c
 *
 *  Created on: Sep 9, 2016
 *      Author: ms
 */

#include "plx_pci.h"

#define buf_len 223*16
#define FileLength  1024
U8 buffer[buf_len];
U8 ReceiveFlag;
BOOL Exam;
BOOL stopexam;
BOOL stopexam1;
BOOL stopexam2;
int WrongNum;
PLX_INTERRUPT	plxInterrupt;
PLX_NOTIFY_OBJECT plxEvent;
BOOL					WaitIntr;
PLX_STATUS openDevice(
	    PLX_DEVICE_KEY    * DeviceKey,
	    PLX_DEVICE_OBJECT * Device)
{
	PLX_STATUS        rc;
	S16               DeviceSelected;
	ConsoleInitialize();

    Cons_clear();

    Cons_printf(
        "\n\n"
        "\t\t            PLX SDK API Test\n"
        "\t\t              January 2007\n\n"
        );
    DeviceSelected = SelectDevice(DeviceKey);

    if (DeviceSelected == -1)
    {
        ConsoleEnd();
        exit(0);
    }

    rc = PlxPci_DeviceOpen(DeviceKey,Device);
    return rc;
};

PLX_STATUS checkApiVersion(
		PLX_DEVICE_OBJECT * Device
){

	U8  VerMajor;
	U8	VerMinor;
	U8	VerRev;
	U16 ChipType;
    U8 Revision;
    PLX_STATUS rc;


	PlxPci_ApiVersion(
    &VerMajor,
    &VerMinor,
    &VerRev
    );
    Cons_printf("PLX SDK API v%d,%d%d \n",VerMajor,VerMinor,VerRev);
    rc = PlxPci_ChipTypeGet(
      Device,
      &ChipType,
      &Revision
    );
    if (rc != PLX_STATUS_OK)
    {

    }
    else
    {
      Cons_printf("ChipType : %04X, Revision: %02X\n", ChipType, Revision);
    }
    return rc;
}

void onReceiverData(
		PLX_DEVICE_OBJECT * Device,
		PLX_DMA_PROP * DmaProp,
		PLX_DMA_PARAMS * dmaParams

){
	U32						flag;

	int						framecount;
	BOOL					b_write;
	PLX_STATUS rc;


	framecount = 0;
	int i = 0;
	WaitIntr = 1;
	b_write = 0;
	//ReceiveFlag = 0xF0;
	ReceiveFlag = 0xF0;

//	rc = PlxPci_PciBarSpaceWrite(Device,1,0x00000004,&LEDSel,1,0,TRUE); // 0x00000004 ?????

	rc = PlxPci_PciBarSpaceWrite(Device,2,0x00001000,&ReceiveFlag,sizeof(ReceiveFlag),BitSize8,TRUE); // 0x00000004 ?????

	if(rc == ApiSuccess){
		Cons_printf("Write ReceiveFlag to PCI local bus\n");
	}else{
		Cons_printf("Write false to PCI local bus\n");
	}

	memset(DmaProp, 0, sizeof(PLX_DMA_PROP));


	DmaProp->ReadyInput			= 1;
    DmaProp->BurstInfinite		= 0;
    DmaProp->Burst				= 1;
    DmaProp->WriteInvalidMode	= 0;
	DmaProp->EnableEOT			= 0;
	DmaProp->FastTerminateMode	= 0;
	DmaProp->ConstAddrLocal		= 1;
	DmaProp->DemandMode			= 0;
	DmaProp->ClearCountMode		= 0;
	DmaProp->WaitStates			= 0;
	DmaProp->LocalBusWidth		= 2;

    rc = PlxPci_DmaChannelOpen(
    		Device,
			0,
			DmaProp
    );

    if(rc == ApiSuccess)
    	Cons_printf("DMA openned \n");
    if(rc == ApiNullParam)
        	Cons_printf("DMA ApiNullParam \n");
    if(rc == ApiInvalidDeviceInfo)
        	Cons_printf("DMA ApiInvalidDeviceInfo \n");
    if(rc == ApiPowerDown)
        	Cons_printf("DMA ApiPowerDown \n");
    if(rc == ApiDmaChannelInvalid)
        	Cons_printf("DMA ApiDmaChannelInvalid \n");
    if(rc == ApiDmaChannelUnavailable)
        	Cons_printf("DMA ApiDmaChannelUnavailable \n");


    dmaParams->UserVa = (U64) buffer;
    dmaParams->LocalAddr = 0x00008000;
    dmaParams->ByteCount = sizeof(buffer);
    dmaParams->Direction = 1;// Local to PCI
    dmaParams->bIgnoreBlockInt = 0;

    while(WaitIntr){
    	memset(buffer, 0, sizeof(buffer));
    	// clear INTR description structure
    	memset(&plxInterrupt, 0, sizeof(PLX_INTERRUPT));
    	memset(&plxEvent, 0, sizeof(PLX_NOTIFY_OBJECT));
    	rc = PlxPci_NotificationStatus(Device, &plxEvent, &plxInterrupt);
    	plxInterrupt.PciMain = 1;
    	plxInterrupt.LocalToPci = 1;
    	plxInterrupt.DmaDone = 1;

    	rc = PlxPci_NotificationRegisterFor(Device, &plxInterrupt, &plxEvent);
    	rc = PlxPci_InterruptEnable(Device,&plxInterrupt);
    	rc = PlxPci_DmaTransferUserBuffer(  Device, // device
    										0,      // channel 0
											dmaParams, // dma params
				                            3*1000 // special time to wait for dma completion
											);
    	if(rc == ApiSuccess){
    		Cons_printf("DmaTransferd buffer = %x%x%x%x\n", buffer[3],buffer[2],buffer[1],buffer[0]);
    		i++;
    	}
    	else if(rc == ApiDmaInProgress){
    		Cons_printf("ApiDmaInProgress\n");
    	}
    	else if(rc == ApiDmaChannelInvalid){
    	    		Cons_printf("ApiDmaChannelInvalid\n");
    	    	}
    	else if(rc == ApiDmaInProgress){
    	    		Cons_printf("ApiDmaInProgress\n");
    	    	}
    	else if(rc == ApiWaitTimeout){
    	    		Cons_printf("ApiWaitTimeout\n");
    	    	}
    	else{
    		Cons_printf("ApiWaitTimeout %d \n",rc);
    	}
//    	if(i == 5){
//    		ReceiveFlag = 0x0F;
//
//    	//	rc = PlxPci_PciBarSpaceWrite(Device,1,0x00000004,&LEDSel,1,0,TRUE); // 0x00000004 ?????
//
//    		rc = PlxPci_PciBarSpaceWrite(Device,2,0x00001000,&ReceiveFlag,sizeof(ReceiveFlag),BitSize8,TRUE); // 0x00000004 ?????
//
//    	}

    }

}

void onStopReceiverData(
		PLX_DEVICE_OBJECT * Device
){
	PLX_STATUS rc;

	//WaitIntr = 0;

	//ReceiveFlag = 0xF0;
	ReceiveFlag = 0x0F;

//	rc = PlxPci_PciBarSpaceWrite(Device,1,0x00000004,&LEDSel,1,0,TRUE); // 0x00000004 ?????

	rc = PlxPci_PciBarSpaceWrite(Device,2,0x00001000,&ReceiveFlag,sizeof(ReceiveFlag),BitSize8,TRUE); // 0x00000004 ?????


	rc = PlxPci_DmaChannelClose(Device, 0);
	if(rc != ApiSuccess){
		Cons_printf("DMA not closed \n");
		rc = PlxPci_DeviceReset(Device);
		if(rc == ApiSuccess){
			Cons_printf("Device Reseted\n");
		}else if(rc == ApiUnsupportedFunction){
			Cons_printf("Not support reset function \n");
		}
		rc = PlxPci_DmaChannelClose(Device, 0);
		if(rc == ApiSuccess){
					Cons_printf("DMA reclosed\n");
		}
	}
	else{
		Cons_printf("DMA closed \n");
	}
}
