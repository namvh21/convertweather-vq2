/*
 * testMakeFile.h
 *
 *  Created on: Sep 9, 2016
 *      Author: ms
 */

#ifndef RECEIVER_PLX_PCI_H_
#define RECEIVER_PLX_PCI_H_
#include <time.h>
#include "Plx.h"
#include "PlxApi.h"
#include "ConsFunc.h"
#include "PlxInit.h"
#include "PlxTypes.h"

PLX_STATUS openDevice(
	    PLX_DEVICE_KEY    * DeviceKey,
	    PLX_DEVICE_OBJECT * Device);
PLX_STATUS checkApiVersion(
		PLX_DEVICE_OBJECT * Device
);
void onReceiverData(
		PLX_DEVICE_OBJECT * Device,
		PLX_DMA_PROP * DmaProp,
		PLX_DMA_PARAMS * dmaParams
);
void onStopReceiverData(
		PLX_DEVICE_OBJECT * Device
);

#endif /* RECEIVER_PLX_PCI_H_ */
