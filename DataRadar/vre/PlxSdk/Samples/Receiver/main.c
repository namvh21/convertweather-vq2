

#include "plx_pci.h"

U8 LEDSel = 0x05;
int  main(void)
{
    PLX_STATUS        rc;
    PLX_DEVICE_KEY    DeviceKey;
    PLX_DEVICE_OBJECT Device;
    PLX_DMA_PROP  DmaProp;
    PLX_DMA_PARAMS dmaParams;
    rc = openDevice(&DeviceKey,&Device);

    if(rc == ApiSuccess){
    	Cons_printf("Device opened!!!\n");
    }
    else{
    	Cons_printf("Device not opened!!!\n");
    }
    Cons_printf("\nSelected: %04x %04x [b:%02x  s:%02x  f:%x]\n\n",DeviceKey.DeviceId, DeviceKey.VendorId,DeviceKey.bus, DeviceKey.slot, DeviceKey.function);

//    rc = PlxPci_PciBarSpaceWrite(&Device,2,0x00000004,&LEDSel,1,0,TRUE); // 0x00000004 ?????
     
    checkApiVersion(&Device);
    onReceiverData(&Device, &DmaProp, &dmaParams);
    onStopReceiverData(&Device);
    return 0;
}



