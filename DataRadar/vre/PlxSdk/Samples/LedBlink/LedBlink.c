 
#include <time.h>
#include "Plx.h"
#include "PlxApi.h"

#if defined(_WIN32)
    #include "..\\Shared\\ConsFunc.h"
    #include "..\\Shared\\PlxInit.h"
#endif

#if defined(PLX_LINUX)
    #include "ConsFunc.h"
    #include "PlxInit.h"
#endif

U8 LEDSel = 0x01;
int  main(void)
{
    S16               DeviceSelected;
    PLX_STATUS        rc;
    PLX_DEVICE_KEY    DeviceKey;
    PLX_DEVICE_OBJECT Device;


    ConsoleInitialize();

    Cons_clear();

    Cons_printf(
        "\n\n"
        "\t\t            PLX SDK API Test\n"
        "\t\t              January 2007\n\n"
        );
    DeviceSelected = SelectDevice(&DeviceKey);

    if (DeviceSelected == -1)
    {
        ConsoleEnd();
        exit(0);
    }

    rc = PlxPci_DeviceOpen(&DeviceKey,&Device);
    if(rc == ApiSuccess) 
    {
	  Cons_printf("Device opened!!!\n");
    }
    Cons_printf("\nSelected: %04x %04x [b:%02x  s:%02x  f:%x]\n\n",DeviceKey.DeviceId, DeviceKey.VendorId,DeviceKey.bus, DeviceKey.slot, DeviceKey.function);

    rc = PlxPci_PciBarSpaceWrite(&Device,2,0x00000004,&LEDSel,1,0,TRUE); // 0x00000004 ?????
   
    if(rc == ApiSuccess)
    {
	  Cons_printf("ApiSuccess!!!\n\n");
    }
    if(rc == ApiInvalidHandle)
    {
	  Cons_printf("ApiInvalidHandle!!!\n\n");
    }
    if(rc == ApiNullParam)
    {
	  Cons_printf("ApiNullParam!!!\n\n");
    }
    if(rc == ApiPowerDown)
    {
	  Cons_printf("ApiPowerDown!!!\n\n");
    }
    if(rc == ApiInsufficientResources)
    {
	  Cons_printf("ApiInsufficientResources!!!\n\n");
    }
    if(rc == ApiInvalidAccessType)
    {
	  Cons_printf("ApiInvalidAccessType!!!\n\n");
    }
    if(rc == ApiInvalidAddress)
    {
	  Cons_printf("ApiInvalidAddress!!!\n\n");
    }
     if(rc == ApiInvalidSize)
    {
	  Cons_printf("ApiInvalidSize!!!\n\n");
    }
    return 0;
}
