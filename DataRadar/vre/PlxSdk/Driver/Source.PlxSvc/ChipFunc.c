/*******************************************************************************
 * Copyright 2013-2015 Avago Technologies
 * Copyright (c) 2009 to 2012 PLX Technology Inc.  All rights reserved.
 *
 * This software is available to you under a choice of one of two
 * licenses.  You may choose to be licensed under the terms of the GNU
 * General Public License (GPL) Version 2, available from the file
 * COPYING in the main directorY of this source tree, or the
 * BSD license below:
 *
 *     Redistribution and use in source and binary forms, with or
 *     without modification, are permitted provided that the following
 *     conditions are met:
 *
 *      - Redistributions of source code must retain the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer.
 *
 *      - Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

/******************************************************************************
 *
 * File Name:
 *
 *      ChipFunc.c
 *
 * Description:
 *
 *      This file contains the PLX chip-specific functions
 *
 * Revision History:
 *
 *      11-01-15 : PLX SDK v7.30
 *
 ******************************************************************************/


#include "ChipFunc.h"
#include "DrvDefs.h"
#include "PciFunc.h"
#include "PciRegs.h"
#include "SuppFunc.h"




/******************************************************************************
 *
 * Function   :  PlxRegisterRead_8111
 *
 * Description:  Reads an 8111 PLX-specific register
 *
 *****************************************************************************/
U32
PlxRegisterRead_8111(
    PLX_DEVICE_NODE *pNode,
    U32              offset,
    PLX_STATUS      *pStatus
    )
{
    U32 value;
    U32 RegSave;


    // Verify register offset
    if ((offset < 0x1000) || (offset > 0x1064))
    {
        if (pStatus != NULL)
            *pStatus = PLX_STATUS_INVALID_OFFSET;
        return 0;
    }

    // Adjust offset
    offset -= 0x1000;

    // Save the current index register
    PLX_PCI_REG_READ(
        pNode,
        0x84,
        &RegSave
        );

    // Set the new index
    PLX_PCI_REG_WRITE(
        pNode,
        0x84,
        offset
        );

    // Get the value
    PLX_PCI_REG_READ(
        pNode,
        0x88,
        &value
        );

    // Restore the current index register
    PLX_PCI_REG_WRITE(
        pNode,
        0x84,
        RegSave
        );

    if (pStatus != NULL)
        *pStatus = PLX_STATUS_OK;

    return value;
}




/******************************************************************************
 *
 * Function   :  PlxRegisterWrite_8111
 *
 * Description:  Writes to an 8111 PLX-specific control register
 *
 *****************************************************************************/
PLX_STATUS
PlxRegisterWrite_8111(
    PLX_DEVICE_NODE *pNode,
    U32              offset,
    U32              value
    )
{
    U32 RegSave;


    // Verify register offset
    if ((offset < 0x1000) || (offset > 0x1064))
        return PLX_STATUS_INVALID_OFFSET;

    // Adjust offset
    offset -= 0x1000;

    // Save the current index register
    PLX_PCI_REG_READ(
        pNode,
        0x84,
        &RegSave
        );

    // Set the new index
    PLX_PCI_REG_WRITE(
        pNode,
        0x84,
        offset
        );

    // Write the value
    PLX_PCI_REG_WRITE(
        pNode,
        0x88,
        value
        );

    // Restore the current index register
    PLX_PCI_REG_WRITE(
        pNode,
        0x84,
        RegSave
        );

    return PLX_STATUS_OK;
}




/******************************************************************************
 *
 * Function   :  PlxRegisterRead_8000
 *
 * Description:  Reads an 8000 PLX-specific register
 *
 *****************************************************************************/
U32
PlxRegisterRead_8000(
    PLX_DEVICE_NODE *pNode,
    U32              offset,
    PLX_STATUS      *pStatus,
    BOOLEAN          bAdjustForPort
    )
{
    int rc;
    U16 Offset_PcieCap;
    U32 value;
    U32 RegValue;
    U32 OffsetAdjustment;


    // Verify that register access is setup
    if (pNode->pRegNode == NULL)
    {
        DebugPrintf(("ERROR: Register access not setup, unable to access PLX registers\n"));

        if (pStatus != NULL)
            *pStatus = PLX_STATUS_INVALID_DATA;
        return 0;
    }

    // Check if BAR 0 has been mapped
    if (pNode->pRegNode->PciBar[0].pVa == NULL)
    {
        DebugPrintf(("Map BAR 0 for PLX reg access\n"));

        // Attempt to map BAR 0
        rc =
            PlxPciBarResourceMap(
                pNode->pRegNode,
                0
                );

        if (rc != 0)
        {
            DebugPrintf(("ERROR: Unable to map BAR 0 for PLX registers\n"));

            if (pStatus != NULL)
                *pStatus = PLX_STATUS_INSUFFICIENT_RES;
            return 0;
        }
    }

    // Adjust offset for port if requested
    if (bAdjustForPort)
    {
        OffsetAdjustment = 0;

        if ((pNode->Key.PlxPortType == PLX_SPEC_PORT_UPSTREAM) ||
            (pNode->Key.PlxPortType == PLX_SPEC_PORT_DOWNSTREAM))
        {
            // Determine port number if hasn't been done
            if (pNode->PortNumber == (U8)-1)
            {
                // Get the offset of the PCI Express capability 
                Offset_PcieCap =
                    PlxGetExtendedCapabilityOffset(
                        pNode,
                        PCI_CAP_ID_PCI_EXPRESS
                        );

                if (Offset_PcieCap == 0)
                {
                    // No PCIe capability, default to port 0
                    pNode->PortNumber = 0;
                }
                else
                {
                    // Get PCIe Link Capabilities
                    PLX_PCI_REG_READ(
                        pNode,
                        Offset_PcieCap + 0x0C,
                        &RegValue
                        );

                    // Get port number
                    pNode->PortNumber = (U8)((RegValue >> 24) & 0xFF);
                }
            }

            // Adjust the offset based on port number
            OffsetAdjustment = (pNode->PortNumber * (4 * 1024));
        }
        else if ((pNode->Key.PlxPortType == PLX_SPEC_PORT_NT_VIRTUAL) ||
                 (pNode->Key.PlxPortType == PLX_SPEC_PORT_NT_LINK))
        {
            // Add base for NT port
            OffsetAdjustment = pNode->Offset_NtRegBase;
        }

        // For MIRA enhanced mode, USB EP regs start at 0 instead of port 3
        if ((pNode->Key.PlxFamily == PLX_FAMILY_MIRA) &&
            (pNode->PciHeaderType == 0) &&
            (pNode->PortNumber == 3))
        {
            DebugPrintf(("Override offset adjust for MIRA USB EP (3000 ==> 0)\n"));
            OffsetAdjustment = 0;
        }

        DebugPrintf((
            "Adjust offset by %02X for port %d\n",
            (int)OffsetAdjustment, pNode->PortNumber
            ));

        offset += OffsetAdjustment;
    }

    // Verify offset
    if (offset >= pNode->pRegNode->PciBar[0].Properties.Size)
    {
        DebugPrintf(("Error - Offset (%02X) exceeds maximum\n", (unsigned)offset));
        if (pStatus != NULL)
            *pStatus = PLX_STATUS_INVALID_OFFSET;
        return 0;
    }

    if (pStatus != NULL)
        *pStatus = PLX_STATUS_OK;

    // For Draco 1, some register cause problems if accessed
    if (pNode->Key.PlxFamily == PLX_FAMILY_DRACO_1)
    {
        if ((offset == 0x856C)  || (offset == 0x8570) ||
            (offset == 0x1056C) || (offset == 0x10570))
        {
            return 0;
        }
    }

    value =
        PHYS_MEM_READ_32(
            pNode->pRegNode->PciBar[0].pVa + offset
            );

    return value;
}




/******************************************************************************
 *
 * Function   :  PlxRegisterWrite_8000
 *
 * Description:  Writes to an 8000 PLX-specific control register
 *
 *****************************************************************************/
PLX_STATUS
PlxRegisterWrite_8000(
    PLX_DEVICE_NODE *pNode,
    U32              offset,
    U32              value,
    BOOLEAN          bAdjustForPort
    )
{
    int rc;
    U16 Offset_PcieCap;
    U32 RegValue;
    U32 OffsetAdjustment;


    // Verify that register access is setup
    if (pNode->pRegNode == NULL)
    {
        DebugPrintf(("ERROR: Register access not setup, unable to access PLX registers\n"));
        return PLX_STATUS_INVALID_DATA;
    }

    // Check if BAR 0 has been mapped
    if (pNode->pRegNode->PciBar[0].pVa == NULL)
    {
        DebugPrintf(("Map BAR 0 for PLX reg access\n"));

        // Attempt to map BAR 0
        rc =
            PlxPciBarResourceMap(
                pNode->pRegNode,
                0
                );

        if (rc != 0)
        {
            DebugPrintf(("ERROR: Unable to map BAR 0 for PLX registers\n"));
            return PLX_STATUS_INSUFFICIENT_RES;
        }
    }

    // Adjust offset for port if requested
    if (bAdjustForPort)
    {
        OffsetAdjustment = 0;

        if ((pNode->Key.PlxPortType == PLX_SPEC_PORT_UPSTREAM) ||
            (pNode->Key.PlxPortType == PLX_SPEC_PORT_DOWNSTREAM))
        {
            // Determine port number if hasn't been done
            if (pNode->PortNumber == (U8)-1)
            {
                // Get the offset of the PCI Express capability 
                Offset_PcieCap =
                    PlxGetExtendedCapabilityOffset(
                        pNode,
                        PCI_CAP_ID_PCI_EXPRESS
                        );

                if (Offset_PcieCap == 0)
                {
                    // No PCIe capability, default to port 0
                    pNode->PortNumber = 0;
                }
                else
                {
                    // Get PCIe Link Capabilities
                    PLX_PCI_REG_READ(
                        pNode,
                        Offset_PcieCap + 0x0C,
                        &RegValue
                        );

                    // Get port number
                    pNode->PortNumber = (U8)((RegValue >> 24) & 0xFF);
                }
            }

            // Adjust the offset based on port number
            OffsetAdjustment = (pNode->PortNumber * (4 * 1024));
        }
        else if ((pNode->Key.PlxPortType == PLX_SPEC_PORT_NT_VIRTUAL) ||
                 (pNode->Key.PlxPortType == PLX_SPEC_PORT_NT_LINK))
        {
            // Add base for NT port
            OffsetAdjustment = pNode->Offset_NtRegBase;
        }

        // For MIRA enhanced mode, USB EP regs start at 0 instead of port 3
        if ((pNode->Key.PlxFamily == PLX_FAMILY_MIRA) &&
            (pNode->PciHeaderType == 0) &&
            (pNode->PortNumber == 3))
        {
            DebugPrintf(("Override offset adjust for MIRA USB EP (3000 ==> 0)\n"));
            OffsetAdjustment = 0;
        }

        DebugPrintf((
            "Adjust offset by %02X for port %d\n",
            (int)OffsetAdjustment, pNode->PortNumber
            ));

        offset += OffsetAdjustment;
    }

    // Verify offset
    if (offset >= pNode->pRegNode->PciBar[0].Properties.Size)
    {
        DebugPrintf(("Error - Offset (%02X) exceeds maximum\n", (unsigned)offset));
        return PLX_STATUS_INVALID_OFFSET;
    }

    // For Draco 1, some register cause problems if accessed
    if (pNode->Key.PlxFamily == PLX_FAMILY_DRACO_1)
    {
        if ((offset == 0x856C)  || (offset == 0x8570) ||
            (offset == 0x1056C) || (offset == 0x10570))
        {
            return PLX_STATUS_OK;
        }
    }

    PHYS_MEM_WRITE_32(
        pNode->pRegNode->PciBar[0].pVa + offset,
        value
        );

    return PLX_STATUS_OK;
}
