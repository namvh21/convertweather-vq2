#!/bin/bash

echo "Updating source list ..."
apt-get update
echo "Upgrading old packages ..."
apt-get upgrade
echo "Installing build-essential ..."
apt-get install build-essential
echo "Installing eclipse-cdt ..."
apt-get install eclipse-cdt
echo "Installing gdb ..."
apt-get install gdb
echo "Installing libudev-dev ..."
apt-get install libudev-dev
echo "Installing telnet service ..."
apt-get install telnet

echo "Installing libusb ..."
cd /home/vre/setup/
tar  -xjvf libusb-1.0.20.tar.bz2
cd libusb-1.0.20
./configure
make
make install
cd ..
rm -rf libusb-1.0.20

echo "Replacing rc.local to /etc/rc.local ..."
cd /home/vre/setup/
chmod +x rc.local
cp -f rc.local /etc/rc.local

echo "Updating count-dount time from grub ..."
cp -f grub /etc/default/grub
update-grub

echo "Deleting unnessarry files and directories ..."
rm -rf /root/Documents/
rm -rf /root/Downloads/
rm -rf /root/Music/
rm -rf /root/Pictures/
rm -rf /root/Public/
rm -rf /root/Templates/
rm -rf /root/Videos/

echo "SETUP FINISH!!!"


